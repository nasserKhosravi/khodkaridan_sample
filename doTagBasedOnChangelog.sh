##getLatestVersions start
versionHeaderLine=$(awk '/^## (0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)?/{ print $0;exit; }' CHANGELOG.md)

if [ "$versionHeaderLine" == "" ]; then
echo "Error: can not find the latest version"
  exit 1
fi

foundVersion=$(echo "$versionHeaderLine" | grep -o -E '(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)')

if [ "$foundVersion" == "" ]; then
echo "Error: the latest version is not found"
  exit 1
fi
echo "extract tag version: $foundVersion"
##getLatestVersions end

##getChangelogByVersion start
foundChangelog=$(awk -v ver="$foundVersion" '
                  /^(##) \[?[0-9]+.[0-9]+.[0-9]+/ {
                     if (p) { exit };
                     if ($2 == ver) {
                         p = 1
                         next
                     }
                 } p
                 ' CHANGELOG.md)
echo "extract changelog: $foundChangelog"
##getChangelogByVersion end

##Add-tag start
git config user.name "bot-${GITLAB_USER_NAME}"
git config user.email "${GITLAB_USER_EMAIL}"
git tag -a "$foundVersion" -m $"$foundChangelog"
echo "Tag created: $foundVersion"
##Add-tag end
