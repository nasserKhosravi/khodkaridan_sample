# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## 1.4.3 - 2019-02-15 
- CHANGEs from 1.4.3
- CHANGEs2 from 1.4.3

## 1.3.0 
- CHANGEs from 1.3.0

## 1.2.1 (2023-09-02)
- CHANGEs from 1.2.1 
